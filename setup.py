from setuptools import setup

setup(
	name='nanoutils',
	version='0.1.0',
	description='Utils used in Nanomori collecion of microservices',
	url='https://bitbucket.org/SlayerGoury/nanoutils',
	author='Goury Gabriev',
	author_email='goury@slayers.ru',
	license='GNU AGPL v3',
	packages=['nanoutils'],
	install_requires=['PyJWT==1.4.2'],
	classifiers=[
		'Development Status :: 2',
		'Intended Audience :: Me',
		'License :: OSI Approved :: GNU Affero General Public License v3',
		'Operating System :: POSIX :: Linux',
		'Programming Language :: Python :: 3.8',
		'Programming Language :: Python :: 3.9',
		'Programming Language :: Python :: 3.10',
	],
)

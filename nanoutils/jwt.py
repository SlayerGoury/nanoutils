import jwt
from jwt.exceptions import DecodeError


class J ():
	def __init__(self, secret):
		self.secret = secret


	def jauth (self, request, anonymous_allowed=False, cookie_name='juser'):
		def decorator (f):
			"""
			This is view decorator that checks JWT and denies access if bad or none
			or calls the view with juser argument if all's good
			"""
			def inner (*args, **kwargs):
				juser = self.read(request, cookie_name=cookie_name)
				if juser is False:
					return 'F', 403
				if juser is None:
					if not anonymous_allowed:
						return 'A', 403
					else:
						juser = {}
				return f(juser=juser, *args, **kwargs)
			return inner
		return decorator


	def read (self, request, cookie_name='juser'):
		juser = request.cookies.get(cookie_name)
		if juser:
			try:
				juser = jwt.decode(juser, self.secret, algorithms=["HS256"])
			except DecodeError:
				return False
		return juser


	def cook_jwt (self, response, cookie_name='juser', **kwargs):
		juser = jwt.encode(kwargs, self.secret, algorithm="HS256")
		response.set_cookie(cookie_name, juser, httponly=True)
		return response
